<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class JobApply extends Model
{
    use Notifiable, HasFactory;

    protected $fillable = [
        'user_id',
        'job_posting_id',
        'curriculum',
        'challenge_date',
        'salary_claim'
    ];
}
