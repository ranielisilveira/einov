<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobPosting extends Model
{
    use HasFactory;
    use SoftDeletes;

    public function jobApplies()
    {
        return $this->hasMany(JobApply::class, 'job_posting_id');
    }
}
