<?php

namespace App\Http\Controllers;

use App\Models\JobApply;
use App\Models\JobPosting;
use App\Notifications\NewJobApply;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class JobApplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  String  $slug
     * @return \Illuminate\Http\Response
     */
    public function create(String $slug)
    {

        $validator = Validator::make(compact('slug'), [
            'slug' => [
                'required',
                Rule::exists('job_postings')->where(function ($query) use ($slug) {
                    return $query->where('slug', $slug)->where('valid_through', '>=', now());
                }),
            ],
        ], [
            'slug.exists' => 'Que pena, está vaga já não está mais disponível.',
        ]);

        if ($validator->fails()) {
            return redirect(route('jobposting.index'))
                ->withErrors($validator);
        }

        $jobposting = JobPosting::where('slug', '=', $slug)->where('valid_through', '>=', now())->first();
        return view('jobapply.create', compact('jobposting'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $slug)
    {
        $request['salary_claim'] = !is_numeric($request->salary_claim)
            ? str_replace(",", ".", str_replace(".", "", $request->salary_claim))
            : $request->salary_claim;

        $request['slug'] = $slug;

        $validator = Validator::make($request->all(), [
            'curriculum' => 'required|file|mimes:pdf',
            'salary_claim' => 'required|numeric',
            'challenge_date' => 'required|date',
            'slug' => [
                'required',
                Rule::exists('job_postings')->where(function ($query) use ($slug) {
                    return $query->where('slug', $slug)->where('valid_through', '>=', now());
                }),
            ],
        ], [
            'curriculum.required' => 'Você deve enviar seu curriculum.',
            'curriculum.file' => 'Você deve enviar um curriculum válido.',
            'curriculum.mimes' => 'Você deve enviar um curriculum válido em formato pdf.',

            'salary_claim.required' => 'Você deve informar sua prentenção salarial.',
            'salary_claim.numeric' => 'Você deve informar sua prentenção salarial corretamente.',

            'challenge_date.required' => 'Você deve informar a data em que deseja receber o desafio.',
            'challenge_date.date' => 'Você deve informar a data em que deseja receber o desafio corretamente.',

            'slug.exists' => 'Que pena, está vaga já não está mais disponível.',
        ]);

        if ($validator->fails()) {
            return redirect()->route('jobapply.create', $slug)->withErrors($validator);
        }

        $file = '';
        if ($request->has('curriculum')) {
            $file = Storage::putFile('public/curriculum', $request->file('curriculum'));
        }

        $jobposting = JobPosting::where('slug', $slug)
            ->with('jobApplies', function ($jobApplies) use ($request) {
                return $jobApplies->where('user_id', $request->user()->id);
            })
            ->first();

        $jobposting->jobApplies->each(function ($apply) {
            $apply->delete();
        });

        $job = JobApply::create([
            'user_id' => $request->user()->id,
            'job_posting_id' => $jobposting->id,
            'curriculum' => $request->curriculum,
            'salary_claim' => $request->salary_claim,
            'challenge_date' => $request->challenge_date,
            'curriculum' => $file,
        ]);

        // $job->notify(new NewJobApply(storage_path($job->curriculum)));
        Notification::route('mail', env('MAIL_FROM_ADDRESS'))->notify(new NewJobApply(storage_path('app/' . $job->curriculum)));

        return view('jobapply.create', compact('jobposting'))->with('message', 'Agradecemos sua candidatura, em breve entraremos em contato.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\JobApply  $jobApply
     * @return \Illuminate\Http\Response
     */
    public function show(JobApply $jobApply)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\JobApply  $jobApply
     * @return \Illuminate\Http\Response
     */
    public function edit(JobApply $jobApply)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\JobApply  $jobApply
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JobApply $jobApply)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\JobApply  $jobApply
     * @return \Illuminate\Http\Response
     */
    public function destroy(JobApply $jobApply)
    {
        //
    }
}
